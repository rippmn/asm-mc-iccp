Steps to perform

**Project Setup**

Note to setup the project and networks use the terraform config in the project-setup directory. The terraform.tfvars file there should be updated for your project and preferred regions.

If this is a new project it can take up to 10 mins for the compute api to be ready for the setup config, so the initial application of the terraform config may fail on the network create step. If it does wait a bit and rerun terrafrom apply.

**IMPORTANT**

As there is not a terraform config (as of 2022-03-08) for enabling the hub mesh feature it must be done after the project setup step completes. 
```
gcloud beta container hub mesh enable
```

**Demo Infra Setup**

Update the terraform.tfvars file for you particular project and desired regions. 

There also is an expectation of a VPC named 'asm-demo' with two subnets in the regions you want to use already exists (created by the config in the project-setup directory). These are loaded by terraform by name which currently indicates the zones I plan to use (west4 and central1) 

If you wish to change this make adjustments in network.tf and subnet references in gce.tf and gke*.tf. You also may wish to update the private cluster cidr blocks and master authorized networks depending on where you place the jumpbox (I have it in central1).

In order to facilitate ssh into node machines an ssh key is set on the node pool machines. The terraform setup expect a ssh key file to be generated in the infra folder named gcedemo (linux and mac use ssh-keygen). If you wish to change this update in the node_config.metadata sections of gke.tf and gke2.tf.

Once you have updated, apply the terraform config.

This entire setup creates 2 clusters ready for the installation of ASM

It includs authorized networks setup here - https://cloud.google.com/service-mesh/docs/unified-install/gke-install-multi-cluster#private-clusters-authorized-network

and the firewall rule requirements here (modifed to use a explictly set tag) - https://cloud.google.com/service-mesh/docs/unified-install/gke-install-multi-cluster#create_firewall_rule

and the global access as described here - https://cloud.google.com/service-mesh/docs/unified-install/gke-install-multi-cluster#enable_control_plane_global_access

and registering the clusters to gke hub and enablement of multicluster ingress feature (note Multi Cluster Ingress can be used for more advanced demos)

and setup a firewall rule to allow sidecar injection see https://cloud.google.com/service-mesh/docs/private-cluster-open-port


follow instructions here (https://cloud.google.com/service-mesh/docs/unified-install/install-dependent-tools) to get the asmcli
(Note that validate expects namespaces to exists which is stupid and the other checks are done via install so can be skipped)

Addtionally as these are private clusters the config creates a jumpbox to run the remainder of the anthos setup from.

A second jump host is created in the second region to be used for more advanced regionall stickiness demos.

**ASM Setup**

Once the Terraform scripts have run ssh into jump host and execute the following steps
```
gcloud compute ssh jump-host --zone us-central1-f
```

Setup Env Vars **NOTE THESE SHOULD BE UPDATED IF YOU HAVE CHANGED THE REGIONS IN terraform.tfvars**
```
export PROJECT_ID=$(gcloud config get-value project)
export PROJECT_1=${PROJECT_ID}
export PROJECT_2=${PROJECT_ID}
export FLEET_PROJECT_ID=${PROJECT_ID}
export LOCATION_1=us-central1-f
export CLUSTER_1=demo-cluster-central
export CTX_1="gke_${PROJECT_1}_${LOCATION_1}_${CLUSTER_1}"
export LOCATION_2=us-west4-c
export CLUSTER_2=demo-cluster-west
export CTX_2="gke_${PROJECT_2}_${LOCATION_2}_${CLUSTER_2}"
export SAMPLES_DIR=istio-1.12.4
alias k=kubectl
```

Get cluster credentials
```
gcloud container clusters get-credentials ${CLUSTER_1} --zone ${LOCATION_1}
gcloud container clusters get-credentials ${CLUSTER_2} --zone ${LOCATION_2}
```


Get the ASM command line installer
```
curl https://storage.googleapis.com/csm-artifacts/asm/asmcli_1.12 > asmcli

chmod +x asmcli
```

Install ASM to clusters
```
./asmcli install   --project_id ${PROJECT_1}   --cluster_name ${CLUSTER_1}   --cluster_location ${LOCATION_1}   --fleet_id ${FLEET_PROJECT_ID}   --output_dir asm_bin --enable_namespace_creation

./asmcli install   --project_id ${PROJECT_2}   --cluster_name ${CLUSTER_2}   --cluster_location ${LOCATION_2}   --fleet_id ${FLEET_PROJECT_ID}   --output_dir asm_bin --enable_namespace_creation
```

Configure endpoint discovery as described here - https://cloud.google.com/service-mesh/docs/unified-install/gke-install-multi-cluster#private-clusters-endpoint
```
PRIV_IP=`gcloud container clusters describe "${CLUSTER_1}" --project "${PROJECT_1}" \
 --zone "${LOCATION_1}" --format "value(privateClusterConfig.privateEndpoint)"`

asm_bin/istioctl x create-remote-secret --context=${CTX_1} --name=${CLUSTER_1} --server=https://${PRIV_IP} > ${CTX_1}.secret

PRIV_IP=`gcloud container clusters describe "${CLUSTER_2}" --project "${PROJECT_2}" \
 --zone "${LOCATION_2}" --format "value(privateClusterConfig.privateEndpoint)"`

asm_bin/istioctl x create-remote-secret --context=${CTX_2} --name=${CLUSTER_2} --server=https://${PRIV_IP} > ${CTX_2}.secret

kubectl apply -f ${CTX_1}.secret --context=${CTX_2}
kubectl apply -f ${CTX_2}.secret --context=${CTX_1}
```

**Multi Cluster Service Sample App Deployment**

Enable sidecar injection and deploy sample
See - https://cloud.google.com/service-mesh/docs/unified-install/gke-install-multi-cluster#enable_sidecar_injection

Get the revision label from istio-system
```
kubectl -n istio-system get pods -l app=istiod --show-labels
```
Determine the label and then set env var LBL and deploy sample

```
export LBL=asm-1124-2
for CTX in ${CTX_1} ${CTX_2}
do
    kubectl create --context=${CTX} namespace sample
    kubectl label --context=${CTX} namespace sample \
        istio-injection- istio.io/rev=$LBL --overwrite
    kubectl --context ${CTX} -n sample run --image=gcr.io/google-samples/whereami:v1.2.6 --expose --port 8080 whereami 
done
```

**Testing MultiCluster Service Discovery**

deploy sleep or nginx to the sample namespace and the remote exec in to curl the whereami.sample:8080/hello endpoint. You shoud see the servicing deployments change from central to west
```
k run nginx -n sample --image nginx --context $CTX_?
```
**Sample Application CLEANUP**

run below (note it also cleans up any nginx pods)

```
for CTX in ${CTX_1} ${CTX_2}
do
    k delete svc whereami -n sample --context ${CTX}
    k delete po whereami -n sample --context ${CTX}
    k delete po nginx -n sample --context ${CTX}
    k delete ns sample --context ${CTX}
done
```