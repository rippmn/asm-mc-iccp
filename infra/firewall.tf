resource "google_compute_firewall" "asm_network_central_west_fw" {
  provider = google-beta
  name    = "asm-central-west-subnet"
  network = data.google_compute_network.vpc_network.name

  direction = "INGRESS"

  priority = "900"

  allow {
    protocol = "tcp"
  }

  source_ranges = [ 
    data.google_compute_subnetwork.demo_subnet_central.ip_cidr_range
  ]

  target_tags = ["asm-node-w4"]

}

resource "google_compute_firewall" "asm_network_west_central_fw" {
  provider = google-beta
  name    = "asm-west-central-subnet"
  network = data.google_compute_network.vpc_network.name

  direction = "INGRESS"

  priority = "900"

  allow {
    protocol = "tcp"
  }

  source_ranges = [ 
    data.google_compute_subnetwork.demo_subnet_west.ip_cidr_range
  ]

  target_tags = ["asm-node-c1"]

}

resource "google_compute_firewall" "asm_multicluster_pods_central_west" {
  provider = google-beta
  name    = "asm-multicluster-pods-central-west"
  network = data.google_compute_network.vpc_network.name

  direction = "INGRESS"

  priority = "900"

  allow {
    protocol = "tcp"
  }

  allow {
    protocol = "udp"
  }

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "esp"
  }

  allow {
    protocol = "ah"
  }

  allow {
    protocol = "sctp"
  }

  source_ranges = [ 
    data.google_compute_subnetwork.demo_subnet_central.secondary_ip_range[0].ip_cidr_range
  ]

  target_tags = ["asm-node-w4"]

}

resource "google_compute_firewall" "asm_multicluster_pods_west_central" {
  provider = google-beta
  name    = "asm-multicluster-pods-west-central"
  network = data.google_compute_network.vpc_network.name

  direction = "INGRESS"

  priority = "900"

  allow {
    protocol = "tcp"
  }

  allow {
    protocol = "udp"
  }

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "esp"
  }

  allow {
    protocol = "ah"
  }

  allow {
    protocol = "sctp"
  }

  source_ranges = [ 
    data.google_compute_subnetwork.demo_subnet_west.secondary_ip_range[0].ip_cidr_range
  ]

  target_tags = [ "asm-node-c1"]

}

resource "google_compute_firewall" "asm_sidecar_inject_west" {
  provider = google-beta
  name    = "asm-sidecar-inject-west"
  network = data.google_compute_network.vpc_network.name

  direction = "INGRESS"

  priority = "901"

  allow {
    protocol = "tcp" 
    ports = ["15017"]
  }

  source_ranges = [ 
    google_container_cluster.demo_cluster_west.private_cluster_config[0].master_ipv4_cidr_block
  ]

  target_tags = [ "asm-node-w4"]

}

resource "google_compute_firewall" "asm_sidecar_inject_central" {
  provider = google-beta
  name    = "asm-sidecar-inject-central"
  network = data.google_compute_network.vpc_network.name

  direction = "INGRESS"

  priority = "901"

  allow {
    protocol = "tcp" 
    ports = ["15017"]
  }

  source_ranges = [ 
    google_container_cluster.demo_cluster_central.private_cluster_config[0].master_ipv4_cidr_block
  ]

  target_tags = [ "asm-node-c1"]

}
