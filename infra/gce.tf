resource "google_compute_instance" "jump" {
  name         = "jump-host"
  machine_type = "e2-micro"
  zone         = var.zone-1
  project      = var.project_id

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }
  metadata_startup_script = "sudo apt update -y; sudo apt install kubectl google-cloud-sdk-gke-gcloud-auth-plugin git jq -y"
  network_interface {
    subnetwork = data.google_compute_subnetwork.demo_subnet_central.self_link

    //access_config {
      // Ephemeral public IP
    //}
  }

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email = google_service_account.asm_install_sa.email
    scopes = ["cloud-platform"]
  }

  shielded_instance_config{
    enable_secure_boot = "true"
  }
}
