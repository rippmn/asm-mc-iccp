resource "google_gke_hub_membership" "asm_central" {
  membership_id = "asm-central"
  project = var.project_id
  endpoint {
    gke_cluster {
      resource_link = "//container.googleapis.com/${google_container_cluster.demo_cluster_central.id}"
    }
  }
  authority {
    issuer = "https://container.googleapis.com/v1/${google_container_cluster.demo_cluster_central.id}"
  }
}

resource "google_gke_hub_feature" "mci-hub-feature" {
  name = "multiclusteringress"
  location = "global"
  spec {
    multiclusteringress {
      config_membership = google_gke_hub_membership.asm_central.id
    }
  }
  provider = google-beta
} 

resource "google_gke_hub_membership" "asm_west" {
  membership_id = "asm-west"
  project = var.project_id
  endpoint {
    gke_cluster {
      resource_link = "//container.googleapis.com/${google_container_cluster.demo_cluster_west.id}"
    }
  }
  authority {
    issuer = "https://container.googleapis.com/v1/${google_container_cluster.demo_cluster_west.id}"
  }
}


