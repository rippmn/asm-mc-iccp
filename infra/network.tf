data "google_compute_network" "vpc_network" {
  provider = google-beta
  name = "asm-demo"

}

data "google_compute_subnetwork" "demo_subnet_central" {
  provider = google-beta
  name          = "asm-demo-c1"
  region = var.region-1
}

data "google_compute_subnetwork" "demo_subnet_west" {
  provider = google-beta
  name          = "asm-demo-w4"
  region = var.region-2
}

resource "google_compute_global_address" "service_ip_address" {
  name = "wai-svc-ip"
  project = var.project_id
}

resource "google_compute_firewall" "ssh" {
  provider = google-beta
  name    = "jump-ssh"
  network = data.google_compute_network.vpc_network.name

  direction = "INGRESS"


  priority = "1000"

  allow {
    protocol = "tcp"
    ports = ["22"]
  }

  source_ranges = [ "0.0.0.0/0"]
}

resource "google_compute_router" "router-central" {
  name    = "central-router"
  network = data.google_compute_network.vpc_network.name
  region  = var.region-1
  project = var.project_id
}

resource "google_compute_router_nat" "nat-central" {
  name                               = "central-router-nat"
  router                             = google_compute_router.router-central.name
  region                             = google_compute_router.router-central.region
  project 			     = var.project_id
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

}

resource "google_compute_router" "router-west" {
  name    = "west-router"
  network = data.google_compute_network.vpc_network.name
  region  = var.region-2
  project = var.project_id
}

resource "google_compute_router_nat" "nat-west" {
  name                               = "west-router-nat"
  router                             = google_compute_router.router-west.name
  region                             = google_compute_router.router-west.region
  project 			     = var.project_id
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

}