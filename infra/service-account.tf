resource "google_service_account" "demo-sa" {
  provider = google-beta
  account_id   = "demo-sa"
  display_name = "demo Service Account"
}

resource "google_service_account" "asm_install_sa" {
  provider = google-beta
  account_id   = "asm-install-sa"
  display_name = "asm installaer Service Account"
}

resource "google_project_iam_member" "demo-sa-editor" {
  provider = google-beta
  project = var.project_id
  role    = "roles/editor"
  member =   format("%s:%s", "serviceAccount", google_service_account.demo-sa.email )
}

resource "google_project_iam_member" "asm_install_hub_admin" {
  provider = google-beta
  project = var.project_id
  role    = "roles/gkehub.admin"
  member =   format("%s:%s", "serviceAccount", google_service_account.asm_install_sa.email )
}

resource "google_project_iam_member" "asm_install_container_admin" {
  provider = google-beta
  project = var.project_id
  role    = "roles/container.admin"
  member =   format("%s:%s", "serviceAccount", google_service_account.asm_install_sa.email )
}

resource "google_project_iam_member" "asm_install_meshconfig_admin" {
  provider = google-beta
  project = var.project_id
  role    = "roles/meshconfig.admin"
  member =   format("%s:%s", "serviceAccount", google_service_account.asm_install_sa.email )
}

resource "google_project_iam_member" "asm_install_resource_mgr_admin" {
  provider = google-beta
  project = var.project_id
  role    = "roles/resourcemanager.projectIamAdmin"
  member =   format("%s:%s", "serviceAccount", google_service_account.asm_install_sa.email )
}

resource "google_project_iam_member" "asm_install_svc_acct_usr" {
  provider = google-beta
  project = var.project_id
  role    = "roles/iam.serviceAccountUser"
  member =   format("%s:%s", "serviceAccount", google_service_account.asm_install_sa.email )
}

resource "google_project_iam_member" "asm_install_svc_mgmt_admin" {
  provider = google-beta
  project = var.project_id
  role    = "roles/servicemanagement.admin"
  member =   format("%s:%s", "serviceAccount", google_service_account.asm_install_sa.email )
}

resource "google_project_iam_member" "asm_install_svc_usg_admin" {
  provider = google-beta
  project = var.project_id
  role    = "roles/serviceusage.serviceUsageAdmin"
  member =   format("%s:%s", "serviceAccount", google_service_account.asm_install_sa.email )
}

resource "google_project_iam_member" "asm_install_privateca_admin" {
  provider = google-beta
  project = var.project_id
  role    = "roles/privateca.admin"
  member =   format("%s:%s", "serviceAccount", google_service_account.asm_install_sa.email )
}

resource "google_project_iam_member" "asm_install_owner" {
  provider = google-beta
  project = var.project_id
  role    = "roles/owner"
  member =   format("%s:%s", "serviceAccount", google_service_account.asm_install_sa.email )
}




