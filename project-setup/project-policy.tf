resource "google_project_organization_policy" "private_cluster_vpc_peering" {
  project = var.project_id
  constraint = "compute.restrictVpcPeering"

  list_policy {
    allow {
      all = true
    }
  }
}
