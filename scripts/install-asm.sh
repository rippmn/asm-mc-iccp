export PROJECT_ID=$(gcloud config get-value project)
export PROJECT_1=${PROJECT_ID}
export PROJECT_2=${PROJECT_ID}
export FLEET_PROJECT_ID=${PROJECT_ID}
export LOCATION_1=us-central1-f
export CLUSTER_1=demo-cluster-central
export CTX_1="gke_${PROJECT_1}_${LOCATION_1}_${CLUSTER_1}"
export LOCATION_2=us-west4-c
export CLUSTER_2=demo-cluster-west
export CTX_2="gke_${PROJECT_2}_${LOCATION_2}_${CLUSTER_2}"
export SAMPLES_DIR=istio-1.12.4
alias k=kubectl

gcloud container clusters get-credentials ${CLUSTER_1} --zone ${LOCATION_1}
gcloud container clusters get-credentials ${CLUSTER_2} --zone ${LOCATION_2}

curl https://storage.googleapis.com/csm-artifacts/asm/asmcli_1.12 > asmcli
chmod +x asmcli

./asmcli install   --project_id ${PROJECT_1}   --cluster_name ${CLUSTER_1}   --cluster_location ${LOCATION_1}   --fleet_id ${FLEET_PROJECT_ID}   --output_dir asm_bin --enable_namespace_creation
./asmcli install   --project_id ${PROJECT_2}   --cluster_name ${CLUSTER_2}   --cluster_location ${LOCATION_2}   --fleet_id ${FLEET_PROJECT_ID}   --output_dir asm_bin --enable_namespace_creation

PRIV_IP=`gcloud container clusters describe "${CLUSTER_1}" --project "${PROJECT_1}" \
 --zone "${LOCATION_1}" --format "value(privateClusterConfig.privateEndpoint)"`

asm_bin/istioctl x create-remote-secret --context=${CTX_1} --name=${CLUSTER_1} --server=https://${PRIV_IP} > ${CTX_1}.secret

PRIV_IP=`gcloud container clusters describe "${CLUSTER_2}" --project "${PROJECT_2}" \
 --zone "${LOCATION_2}" --format "value(privateClusterConfig.privateEndpoint)"`

asm_bin/istioctl x create-remote-secret --context=${CTX_2} --name=${CLUSTER_2} --server=https://${PRIV_IP} > ${CTX_2}.secret

kubectl apply -f ${CTX_1}.secret --context=${CTX_2}
kubectl apply -f ${CTX_2}.secret --context=${CTX_1}